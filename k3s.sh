#!/bin/bash

curl -sfL https://get.k3s.io |  INSTALL_K3S_VERSION=v1.27.6+k3s1 sh -s - server \
    --node-ip="192.168.10.4" --flannel-iface="enp0s8"